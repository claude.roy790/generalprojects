﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Imaging;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace ColorGetter
{
    public class Program
    {
        static int nbiterations = 1000;
        static string fileName = "C:/fullScreen-4.png";
        static int nbTest = 4;
        static bool useImage = false;
        static bool useCopyFromScreen = false;

        static void Main(string[] args)
        {
            Random random = new Random();
            HashSet<int> hash = new HashSet<int>();
            for (int i = 0; i < nbTest; i++)
            {
                int nb = 0;
                bool valid = false;
                while (!valid)
                {
                    nb = random.Next(nbTest);
                    if (hash.Add(nb))
                        valid = true;
                }
                switch (nb)
                {
                    case 0:
                        Console.WriteLine("==LockBitmap Map==");
                        DoTest(LockBitmapTest);
                        break;
                    case 1:
                        Console.WriteLine("======BMP SSNOOP=====");
                        DoTest(BmpSnoopPixelTest);
                        break;
                    case 2:
                        Console.WriteLine("==Direct Map==");
                        DoTest(DirectBitmapTest);
                        break;
                    case 3:
                        Console.WriteLine("==Bitmap==");
                        DoTest(BitmapTest);
                        break;
                }
            }

            Console.WriteLine("test done");
            Console.ReadLine();
        }

        static void DoTest(Action<Bitmap> action)
        {
            Bitmap bitmap = new Bitmap(Image.FromFile(fileName));
            Bitmap bm = new Bitmap(bitmap.Width, bitmap.Height);
            for (int i = 0; i < bitmap.Width; i++)
            {
                for (int j = 0; j < bitmap.Height; j++)
                {
                    bm.SetPixel(i, j, bitmap.GetPixel(i, j));
                }
            }
            using (var bmp = bm)
            {
                Stopwatch watch = new Stopwatch();
                try
                {
                    watch.Start();
                    action(bmp);
                    watch.Stop();
                    System.Console.WriteLine($"Took {watch.ElapsedMilliseconds} ms to run");
                }
                catch (Exception ex)
                {
                    Console.WriteLine($"Test failed!: {watch.ElapsedMilliseconds} ex: {ex.Message}");
                }
                watch.Stop();
            }
            bitmap.Dispose();
            bm.Dispose();
        }

        private static void BitmapTest(Bitmap bmp)
        {
            Random rnd = new Random();
            int currentIterations = 0;
            while (currentIterations++ < nbiterations)
            {
                int x = useImage ? rnd.Next(bmp.Width) : rnd.Next(1024);
                int y = useImage ? rnd.Next(bmp.Height) : rnd.Next(768);
                if (useImage)
                {
                    Color c = bmp.GetPixel(x, y);
                }
                else
                {
                    if (!useCopyFromScreen)
                    {
                        GetColorAt(x, y);
                        continue;
                    }
                    Bitmap bitmap = new Bitmap(1, 1);
                    Rectangle bounds = new Rectangle(x, y, 1, 1);
                    Color col = bmp.GetPixel(x, y);
                    using (Graphics g = Graphics.FromImage(bitmap))
                        g.CopyFromScreen(bounds.Location, Point.Empty, bounds.Size);
                    col = bmp.GetPixel(0, 0);
                }
            }
        }

        static void DirectBitmapTest(Bitmap bitmap)
        {
            Random rnd = new Random();
            int currentIterations = 0;
            while (currentIterations++ < nbiterations)
            {
                int x = useImage ? rnd.Next(bitmap.Width) : rnd.Next(1024);
                int y = useImage ? rnd.Next(bitmap.Height) : rnd.Next(768);
                if (useImage)
                {
                    DirectBitmap direct = new DirectBitmap(bitmap.Width, bitmap.Height);
                    Color col = direct.GetPixel(x, y);
                }
                else
                {
                    try
                    {
                        if (!useCopyFromScreen)
                        {
                            GetColorAt(x, y);
                            continue;
                        }
                        DirectBitmap direct = new DirectBitmap(1, 1);
                        Rectangle bounds = new Rectangle(x, y, 1, 1);
                        using (Graphics g = Graphics.FromImage(direct.Bitmap))
                                g.CopyFromScreen(bounds.Location, Point.Empty, bounds.Size);
 
                        Color col = direct.GetPixel(0, 0);
                    }
                    catch
                    {

                    }
                }
            }
        }

        static void BmpSnoopPixelTest(Bitmap bitmap)
        {
            Random rnd = new Random();
            int currentIterations = 0;
            while (currentIterations++ < nbiterations)
            {
                int x = useImage ? rnd.Next(bitmap.Width) : rnd.Next(1024);
                int y = useImage ? rnd.Next(bitmap.Height) : rnd.Next(768);
                if (useImage)
                {
                    BmpPixelSnoop direct = new BmpPixelSnoop(bitmap);
                    Color col = direct.GetPixel(x, y);

                }
                else
                {
                    try
                    {

                        if (!useCopyFromScreen)
                        {
                            GetColorAt(x, y);
                            continue;
                        }

                        Bitmap bmp = new Bitmap(1, 1);
                        BmpPixelSnoop snoop = new BmpPixelSnoop(bmp);
                        Rectangle bounds = new Rectangle(x, y, 1, 1);
                        using (Graphics g = Graphics.FromImage(bmp))
                            g.CopyFromScreen(bounds.Location, Point.Empty, bounds.Size);
                        Color col = snoop.GetPixel(0, 0);

                    }
                    catch
                    {

                    }
                }
            }
        }

        static void LockBitmapTest(Bitmap bitmap)
        {
            int currentIterations = 0;
            Random rnd = new Random();
            while (currentIterations++ < nbiterations)
            {
                int x = useImage ? rnd.Next(bitmap.Width) : rnd.Next(1024);
                int y = useImage ? rnd.Next(bitmap.Height) : rnd.Next(768);
                if (useImage)
                {
                    LockBitmap lck = new LockBitmap(bitmap);
                    lck.LockBits();
                    Color col = lck.GetPixel(x, y);
                }
                else
                {
                    try
                    {
                        if (!useCopyFromScreen)
                        {
                            GetColorAt(x, y);
                            continue;
                        }

                        Bitmap bmp = new Bitmap(1, 1);
                        LockBitmap lck = new LockBitmap(bmp);
                        lck.LockBits();
                        Rectangle bounds = new Rectangle(x, y, 1, 1);
                        using (Graphics g = Graphics.FromImage(bmp))
                            g.CopyFromScreen(bounds.Location, Point.Empty, bounds.Size);
                        Color col = lck.GetPixel(0, 0);
                    }
                    catch
                    {

                    }
                }
            }
        }

        public static Color GetColorAt(int x,int y)
        {
            Bitmap screenPixel = new Bitmap(1, 1, PixelFormat.Format32bppArgb);
            using (Graphics gdest = Graphics.FromImage(screenPixel))
            {
                using (Graphics gsrc = Graphics.FromHwnd(IntPtr.Zero))
                {
                    IntPtr hSrcDC = gsrc.GetHdc();
                    IntPtr hDC = gdest.GetHdc();
                    int retval = BitBlt(hDC, 0, 0, 1, 1, hSrcDC, x, y, (int)CopyPixelOperation.SourceCopy);
                    gdest.ReleaseHdc();
                    gsrc.ReleaseHdc();
                }
            }

            return screenPixel.GetPixel(0, 0);
        }


        [DllImport("gdi32.dll", CharSet = CharSet.Auto, SetLastError = true, ExactSpelling = true)]
        public static extern int BitBlt(IntPtr hDC, int x, int y, int nWidth, int nHeight, IntPtr hSrcDC, int xSrc, int ySrc, int dwRop);
    }

}
